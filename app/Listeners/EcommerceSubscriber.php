<?php

namespace App\Listeners;

class EcommerceSubscriber
{
    /**
     * Handle user login events.
     */
    public function onOrderCreated($event) {
        // newly created Order: $event->order
        // project specific actions when new order is created
    }

    public function onOrderStatusChanged($event) {
        // (\Insolutions\Ecommerce\Order) $event->order
        // (\Insolutions\Ecommerce\OrderStatus) $event->newStatus
        // (\Insolutions\Ecommerce\OrderStatus) $event->oldStatus
        //print 'STATUS CHANGE: Order #' . $event->order->id . ' Status ' . $event->oldStatus->name . ' -> ' . $event->newStatus->name; exit;
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Insolutions\Ecommerce\EventOrderCreated',
            'App\Listeners\EcommerceSubscriber@onOrderCreated'
        );

        $events->listen(
            'Insolutions\Ecommerce\EventOrderStatusChanged',
            'App\Listeners\EcommerceSubscriber@onOrderStatusChanged'
        );

    }

}