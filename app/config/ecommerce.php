<?php

return [

    /*
    |--------------------------------------------------------------------------
    | ins/ecommerce config gile
    |--------------------------------------------------------------------------
    |
    | 
    |
    */
   
    'gopay_goid' => env('GOPAY_GOID'),
    'gopay_clientId' => env('GOPAY_CLIENT_ID'),
    'gopay_clientSecret' => env('GOPAY_CLIENT_SECRET'),
    'gopay_isProductionMode' => env('GOPAY_PRODUCTION', false),

];
