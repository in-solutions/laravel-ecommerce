<?php

namespace Insolutions\Ecommerce;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;

class EcommerceController extends Controller
{
 
    private static function getGoPayInstance() {
        return \GoPay\Api::payments([
            'goid' => config('ecommerce.gopay_goid'),
            'clientId' => config('ecommerce.gopay_clientId'),
            'clientSecret' => config('ecommerce.gopay_clientSecret'),
            'isProductionMode' => config('ecommerce.gopay_isProductionMode')
        ]);
    }

    public function gopayNotification(Request $r) {
        $this->checkGoPayPayment($r->paymentSessionId);
    }

    public function checkPayment(Request $r, $payment_id) {
        list($gopay_res, $order) = $this->checkGoPayPayment($payment_id);

        return response()->json([
            'gopay_payment' => $gopay_res->json,
            'order' => $order
        ]);
    }

    private function checkGoPayPayment($goPaySessionId) {
        $gopay = self::getGoPayInstance();
        $gopay_response = $gopay->getStatus($goPaySessionId);

        $statusToChange = null;
        switch ($gopay_response->json['state']) {
            case 'PAID':
                $order = Order::find($gopay_response->json['order_number']);
                if ($order) {
                    $order->postPaidAction();
                }
                break;
        }

        return [$gopay_response, (isset($order) ? $order : null)];
    }

    public function payOrder(Request $r, $order_id) {
        $order = Order::findOrFail($order_id);

        // === [GOPAY] ===
        $gopay = self::getGoPayInstance();
        $gopay_response = $gopay->createPayment([
            'amount' => 100 * $order->total_price_with_vat,   // converts to long 
            'currency' => $order->currency->code,
            'order_number' => $order->id,
            'items' => [
                [
                    'name' => $order->items[0]->product->translation->title,
                    'amount' => 100 * $order->items[0]->getTotalPriceWithVat()
                ],
            ],
            'callback' => [
                'return_url' => ($r->return_url ? $r->return_url : route('ecommerce.gopay_return')),
                'notification_url' => route('ecommerce.gopay_notification')
            ]
        ]);
        
        if (!$gopay_response->hasSucceed()) {
            // errors format: https://doc.gopay.com/en/?shell#http-result-codes
            var_dump($gopay_response);
            return response("GoPay request failed", 500);
        }

        $response_data = [
            'payment_url' => $gopay_response->json['gw_url'],
            'payment_provider' => 'gopay'
        ];

        return response()->json($response_data);
    }

    public function getOrderStatus(Request $r, $order_id) {

        $order = Order::findOrFail($order_id);

        return response()->json([
            'order_status' => $order->status()->get()
        ]);
    }

    public function getProducts(Request $r)
    {
    	$result = Product::paginate($r->perPage ?: 50);

        return response()->json($result);
    }

    public function submitOrder(Request $r) {
    	$order = Order::createForUser(Auth::user());

    	if (is_array($r->cart)) {
    		foreach ($r->cart as $item) {
    			$order->addProduct(
    				Product::findOrFail($item['product_id']),
    				$item['quantity'] ?: 1
    			);
    		}
    	}        

    	event(new EventOrderCreated($order));

        if ($order->total_price_with_vat == 0) {
            $order->changeStatus(OrderStatus::STATUS_ACCEPTED);
        }

    	$result = Order::where(['id' => $order->id])
    		->with('items')
    		->first();

    	return response()->json($result);
    }

}