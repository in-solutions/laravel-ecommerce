<?php

Route::group(['prefix' => 'ecommerce'], function () {
	
	Route::get('product', 'Insolutions\Ecommerce\EcommerceController@getProducts');

	Route::post('order', 'Insolutions\Ecommerce\EcommerceController@submitOrder');

	Route::get('order/{order_id}/status', 'Insolutions\Ecommerce\EcommerceController@getOrderStatus');

	Route::get('order/{order_id}/paymentUrl', 'Insolutions\Ecommerce\EcommerceController@payOrder');	

	Route::get('gopay/checkPayment/{payment_id}', 'Insolutions\Ecommerce\EcommerceController@checkPayment');

	Route::get('gopayNotification', 'Insolutions\Ecommerce\EcommerceController@gopayNotification')->name('ecommerce.gopay_notification');
	Route::get('gopayReturn', function () { return redirect('/'); })->name('ecommerce.gopay_return');

});