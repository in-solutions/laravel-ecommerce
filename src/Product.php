<?php

namespace Insolutions\Ecommerce;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Insolutions\Items\ItemTrait;

class Product extends Model
{
    use SoftDeletes, ItemTrait;
	
	protected $table = 't_product';
	
	protected $fillable = [		
	];

	protected $hidden = [
        'created_at',
		'updated_at',
		'deleted_at'
    ];

	protected $appends = [
		'translation',
		'price'
	];

	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function currency() {
		return $this->belongsTo('Insolutions\Ecommerce\Currency', 'currency_code', 'code');
	}

	public function getPrice(Currency $currency = null) {
		$q = $this->prices();
		if (!is_null($currency)) {
			$q = $q->where('currency_code', $currency->code);
		}
		
		return $q->orderBy('created_at','desc')->first();
	}

	public function getTranslationAttribute() {
		return $this->translations()/*->where('language', 'sk')*/->first();
	}

	public function getPriceAttribute() {
		return $this->getPrice();
	}

	public function prices() {
		return $this->hasMany('Insolutions\Ecommerce\ProductPrice');
	}
	
	public function translations() {
		return $this->hasMany('Insolutions\Ecommerce\ProductTranslation');
	}
}
