<?php

namespace Insolutions\Ecommerce;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{	
	protected $table = 'enm_order_status';
	
	protected $fillable = [
		'name',
		'default'
	];

	public $timestamps = false;

	const STATUS_NEW		= 1;
	const STATUS_ACCEPTED	= 2;

	public static function getDefault() {
		return self::where(['default' => 1])->first();
	}

}
