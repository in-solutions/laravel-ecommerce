<?php

namespace Insolutions\Ecommerce;

use Insolutions\Ecommerce\Order;
use Illuminate\Queue\SerializesModels;

class EventOrderCreated
{
    use SerializesModels;

    public $order;

    /**
     * Create a new event instance.
     *
     * @param  Order  $order
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }
}