<?php

namespace Insolutions\Ecommerce;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{	
	protected $table = 'enm_currency';
	protected $primaryKey = 'code'; 
	public $incrementing = false;

	public $timestamps = false;
	
}
