<?php

namespace Insolutions\Ecommerce;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;

class Order extends Model
{
    use SoftDeletes;
	
	protected $table = 't_order';
	
	protected $fillable = [	
	];

	protected $hidden = [
		'deleted_at'
    ];

	protected $appends = [
	];

	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function postPaidAction() {
		$this->changeStatus(OrderStatus::STATUS_ACCEPTED);
	}

	public static function createForUser($user) {
		$o = new self;
		$o->user()->associate($user);
		$o->status()->associate(OrderStatus::getDefault());
		$o->currency()->associate(Currency::first());
		$o->save();
		return $o;
	}

	public function setCurrency(Currency $currency) {
		$this->currency()->associate($currency);
		return $this;
	}

	public function changeStatus($newStatus) {
		if (is_numeric($newStatus)) {
			$newStatus = OrderStatus::find($newStatus);
		} elseif (get_class($newStatus) != 'OrderStatus') {
			throw new InvalidArgumentException('First argument must be OrderStatus object or (int) ID of existing Order Status model');
		}

		$oldStatus = $this->status;

		// if is it same as current status skip
		if ($oldStatus->id == $newStatus->id) {
			return;
		}

		$this->status()->associate($newStatus);
		$this->save();
		event(New EventOrderStatusChanged($this, $newStatus, $oldStatus));
	}

	public function addProduct(Product $product, $quantity = 1, ProductPrice $price = null) {
		$oi = OrderItem::createForOrder($this);

		$oi->setProduct($product);
		$oi->setQuantity($quantity);

		if (is_null($price)) {
			$price = $product->getPrice($this->currency);
		}

		if ($price) {
			$oi->setPrice($price);
		}

		$oi->save();

		$this->recalculateTotalSum();

		return $oi;
	}

	private function recalculateTotalSum() {
		$this->total_price_wo_vat = $this->total_price_with_vat = 0;

		foreach ($this->items as $orderItem) {
			$this->total_price_wo_vat += $orderItem->getTotalPriceWoVat();
			$this->total_price_with_vat += $orderItem->getTotalPriceWithVat();
		}

		$this->save();
	}

	public function currency() {
		return $this->belongsTo('Insolutions\Ecommerce\Currency', 'currency_code', 'code');
	}

	public function user() {
		return $this->belongsTo('App\User');
	}

	public function status() {
		return $this->belongsTo('Insolutions\Ecommerce\OrderStatus', 'status_id');
	}

	public function items() {
		return $this->hasMany('Insolutions\Ecommerce\OrderItem');
	}
}
