<?php

namespace Insolutions\Ecommerce;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProductPrice extends Model
{
    use SoftDeletes;
	
	protected $table = 't_product_price';
	
	protected $fillable = [
		'product_id',
		'vat_rate',
		'currency_code',
	];

	protected $hidden = [
		'id',
		'product_id',
        'created_at',
		'updated_at',
		'deleted_at'
    ];

	protected $guarded = [];

	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];
	
	public function product() {
		return $this->belongsTo('Insolutions\Ecommerce\Product');
	}

	public function currency() {
		return $this->belongsTo('Insolutions\Ecommerce\Currency', 'currency_code', 'code');
	}
}
