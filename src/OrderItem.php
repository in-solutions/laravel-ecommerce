<?php

namespace Insolutions\Ecommerce;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{	
	protected $table = 't_order_item';
	
	protected $fillable = [
		'order_id',
		'product_id',
		'quantity'		
	];

	protected $hidden = [
		'order_id'
	];

	protected $attributes = [
        'quantity' => 1,
        'unit_price_wo_vat' => 0,
        'unit_price_with_vat' => 0,
        'vat_rate' => 0,
    ];

	public $timestamps = false;

	public function getTotalPriceWoVat() {
		return $this->unit_price_wo_vat * $this->quantity;
	}

	public function getTotalPriceWithVat() {
		return $this->unit_price_with_vat * $this->quantity;
	}

	public static function createForOrder(Order $order) {
		$oi = new self;
		$oi->order()->associate($order);

		return $oi;
	}

	public function setProduct(Product $product) {
		$this->product()->associate($product);
		if ($productPrice = $product->getPrice($this->order->currency)) {
			$this->setPrice($productPrice);
		}
	}

	public function setQuantity($quantity) {
		$this->quantity = $quantity;
	}

	public function setPrice(ProductPrice $price) {
		$this->unit_price_wo_vat = $price->unit_price_wo_vat;
		$this->unit_price_with_vat = $price->unit_price_with_vat;
		$this->vat_rate = $price->vat_rate;		
	}

	public function order() {
		return $this->belongsTo('Insolutions\Ecommerce\Order');
	}

	public function product() {
		return $this->belongsTo('Insolutions\Ecommerce\Product');
	}

}
