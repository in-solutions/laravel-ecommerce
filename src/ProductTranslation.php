<?php

namespace Insolutions\Ecommerce;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{    

	protected $table = 't_product_tsl';
	
	protected $fillable = [
		'product_id',
		'language',
		'title',
		'description',
	];

	protected $hidden = [
        'id',
        'product_id',
        'language'
    ];

	protected $guarded = [];

	public $timestamps = false;
	
	public function product() {
		return $this->belongsTo('Insolutions\Ecommerce\Product');
	}
}
