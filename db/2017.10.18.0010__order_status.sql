ALTER TABLE `t_order` ADD `status_id` INT UNSIGNED NOT NULL AFTER `user_id`, ADD INDEX (`status_id`) ;


CREATE TABLE IF NOT EXISTS `enm_order_status` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `enm_order_status`
 ADD PRIMARY KEY (`id`);

INSERT INTO `enm_order_status` (`id`, `name`, `default`) VALUES 
('1', 'new', '1'), 
('2', 'accepted', '0');


-- DEFAULT VALUE OF STATUS for EXISTING ORDER to ACCEPTED
UPDATE `t_order` SET `status_id` = 2;

-- SET FOREIGN KEY
ALTER TABLE `t_order` ADD FOREIGN KEY (`status_id`) REFERENCES `enm_order_status`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
