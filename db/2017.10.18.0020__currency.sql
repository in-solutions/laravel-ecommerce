CREATE TABLE IF NOT EXISTS `enm_currency` (
  `code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `symbol` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `symbol_position` enum('before','after') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `enm_currency`
 ADD PRIMARY KEY (`code`);


INSERT INTO `enm_currency` (`code`, `symbol`, `symbol_position`) VALUES
('EUR', '€', 'after');