CREATE TABLE IF NOT EXISTS `enm_parameter` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('value','range','text','presence') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `t_product_parameter` (
`id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `parameter_id` int(11) unsigned NOT NULL,
  `number_from` decimal(20,4) DEFAULT NULL,
  `number_to` decimal(20,4) DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `enm_parameter`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `t_product_parameter`
 ADD PRIMARY KEY (`id`), ADD KEY `product_id` (`product_id`), ADD KEY `parameter_id` (`parameter_id`);

ALTER TABLE `enm_parameter` ADD UNIQUE(`name`);

ALTER TABLE `t_product_parameter`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `t_product_parameter`
ADD CONSTRAINT `t_product_parameter_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `t_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `t_product_parameter_ibfk_2` FOREIGN KEY (`parameter_id`) REFERENCES `enm_parameter` (`id`) ON UPDATE CASCADE;
