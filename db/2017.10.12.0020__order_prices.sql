ALTER TABLE `t_order_item` 
	ADD `unit_price_wo_vat` DECIMAL(20,4) NOT NULL ,
	ADD `unit_price_with_vat` DECIMAL(20,4) NOT NULL , 
	ADD `vat_rate` DECIMAL(10,4) NOT NULL ;

ALTER TABLE `t_order` 
	ADD `total_price_wo_vat` DECIMAL(20,4) NOT NULL ,
	ADD `total_price_with_vat` DECIMAL(20,4) NOT NULL ;

