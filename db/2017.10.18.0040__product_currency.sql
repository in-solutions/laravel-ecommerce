ALTER TABLE `t_product_price` CHANGE `currency` `currency_code` VARCHAR(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE `t_product_price` ADD INDEX(`currency_code`);

ALTER TABLE `t_product_price` ADD FOREIGN KEY (`currency_code`) REFERENCES `enm_currency`(`code`) ON DELETE RESTRICT ON UPDATE CASCADE;