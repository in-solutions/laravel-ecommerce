-- creates items for products
INSERT INTO t_item (entity, entity_id) SELECT "INS\\Ecommerce\\Product", id FROM `t_product`;

-- create item parameters from product parameters
INSERT INTO t_item_parameter (item_id, parameter_id, number_from, number_to, text) 
SELECT i.id, parameter_id, number_from, number_to, text 
FROM t_product_parameter as pp 
JOIN t_item as i ON pp.product_id = i.entity_id AND i.entity = "INS\\Ecommerce\\Product";

DROP TABLE t_product_parameter;