ALTER TABLE `t_order` ADD `currency_code` VARCHAR(3) NOT NULL AFTER `total_price_with_vat`;

ALTER TABLE `t_order` ADD INDEX(`currency_code`);

UPDATE `t_order` SET currency_code = 'EUR';

ALTER TABLE `t_order` ADD FOREIGN KEY (`currency_code`) REFERENCES `enm_currency`(`code`) ON DELETE RESTRICT ON UPDATE CASCADE;